package co.com.example.employees.controllers;

import co.com.example.employees.entities.Employee;
import co.com.example.employees.models.EmployeeModel;
import co.com.example.employees.repositories.EmployeeRepository;
import co.com.example.employees.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Optional;

@RestController
@RequestMapping("/employee")
public class EmployeeController {


    private final EmployeeService employeeService;
    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeController(EmployeeService employeeService, EmployeeRepository employeeRepository) {
        this.employeeService = employeeService;
        this.employeeRepository = employeeRepository;
    }

    @GetMapping()
    public Page<Employee> getEmployees(@RequestParam Optional<Integer> page, @RequestParam Optional<String> sortBy) {
        return employeeRepository.findAll(PageRequest.of(page.orElse(0), 10, Sort.Direction.ASC, sortBy.orElse("id")));
    }

    @PostMapping()
    public ResponseEntity<EmployeeModel> saveEmployee(@RequestBody @Valid EmployeeModel employeeModel) throws URISyntaxException {
        EmployeeModel employee = employeeService.saveEmployee(employeeModel);
        if (employee == null) {
            return ResponseEntity.notFound().build();
        } else {
            URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(employee.getId())
                    .toUri();
            return ResponseEntity.created(uri)
                    .body(employee);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<EmployeeModel> updateEmployee(@RequestBody EmployeeModel employeeModel, @PathVariable Long id) {
        EmployeeModel employee = employeeService.saveEmployee(employeeModel);
        if (employee == null) {
            return ResponseEntity.notFound().build();
        } else {
            URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(employee.getId())
                    .toUri();
            return ResponseEntity.created(uri)
                    .body(employee);
        }
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<EmployeeModel> deleteEmployee(@PathVariable Long id) {
        employeeService.deleteEmployee(id);
        return ResponseEntity.noContent().build();
    }
}
