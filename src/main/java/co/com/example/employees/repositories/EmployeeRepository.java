package co.com.example.employees.repositories;

import co.com.example.employees.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    public abstract ArrayList<Employee> findEmployeeByEmail(String email);

    public abstract Employee findEmployeeByDoc(String doc);
}
