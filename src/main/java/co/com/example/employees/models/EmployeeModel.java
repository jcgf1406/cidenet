package co.com.example.employees.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmployeeModel {
    private Long id;
    private String firstSurname;
    private String secondSurname;
    private String firstName;
    private String otherName;
    private String country;
    private String docType;
    private String doc;
    private String email;
    private Date admissionDate;
    private String area;
    private Boolean status;
    private Date registrationDate;
}
