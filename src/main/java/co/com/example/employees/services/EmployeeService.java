package co.com.example.employees.services;

import co.com.example.employees.entities.Employee;
import co.com.example.employees.mappers.EmployeeMapper;
import co.com.example.employees.models.EmployeeModel;
import co.com.example.employees.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final EmployeeMapper employeeMapper;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository, EmployeeMapper employeeMapper) {
        this.employeeRepository = employeeRepository;
        this.employeeMapper = employeeMapper;
    }

    private ArrayList<Employee> getEmployeeByEmail(String email) {
        return (ArrayList<Employee>) employeeRepository.findEmployeeByEmail(email);
    }

    public EmployeeModel saveEmployee(EmployeeModel employeeModel) {
        Employee employee = employeeMapper.toEmployee(employeeModel);
        if (employeeRepository.findEmployeeByDoc(employee.getDoc()) == null) {
            employee.setFirstSurname(employee.getFirstSurname().toUpperCase());
            employee.setSecondSurname(employee.getSecondSurname().toUpperCase());
            employee.setFirstName(employee.getFirstName().toUpperCase());
            employee.setOtherName(employee.getOtherName().toUpperCase());
            employee.setEmail(generateEmail(employee.getFirstName(), employee.getFirstSurname()));
            employee.setStatus(true);
            employee.setRegistrationDate(new Date());
            employeeRepository.save(employee);
            return employeeMapper.toEmployeeModel(employee);
        } else {
            return null;
        }
    }

    public boolean deleteEmployee(Long id) {
        try {
            employeeRepository.deleteById(id);
            return true;
        } catch (Exception err) {
            return false;
        }
    }

    private String generateEmail(String firstName, String firstSurname) {
        String domain = "@CIDENET.COM.CO";
        String email = firstName + "." + firstSurname + domain;
        if (getEmployeeByEmail(email).size() > 0) {
            int cont = 1;
            email = firstName + "." + firstSurname + "." + cont + domain;
            while (getEmployeeByEmail(email).size() > 0) {
                cont++;
                email = firstName + "." + firstSurname + "." + cont + domain;
            }
        }
        return email;
    }
}
