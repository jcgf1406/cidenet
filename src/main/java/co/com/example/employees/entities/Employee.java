package co.com.example.employees.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", unique = true)
    private Long id;
    @Basic(optional = false)
    @Column(name = "first_surname")
    @Size(min = 0, max = 20, message = "The maximum size is 20")
    @Pattern(regexp = "[A-Z]+", message = "Can only have letters")
    private String firstSurname;
    @Basic(optional = false)
    @Column(name = "second_surname")
    @Size(min = 0, max = 20, message = "The maximum size is 20")
    @Pattern(regexp = "[A-Z]+", message = "Can only have letters")
    private String secondSurname;
    @Basic(optional = false)
    @Column(name = "first_name")
    @Size(min = 0, max = 20, message = "The maximum size is 20")
    @Pattern(regexp = "[A-Z]+", message = "Can only have letters")
    private String firstName;
    @Column(name = "other_name")
    @Size(min = 0, max =  50, message = "The maximum size is 50")
    @Pattern(regexp = "[A-Z]+", message = "Can only have letters")
    private String otherName;
    @Basic(optional = false)
    @Column(name = "country")
    private String country;
    @Basic(optional = false)
    @Column(name = "doc_type")
    private String docType;
    @Basic(optional = false)
    @Column(name = "doc", unique = true)
    @Size(min = 0, max = 20, message = "The maximum size is 20")
    @Pattern(regexp = "[a-zA-Z0-9]+", message = "invalid data")
    private String doc;
    @Basic(optional = false)
    @Lob
    @Column(name = "email")
    @Size(min = 0, max = 300, message = "The maximum size is 300")
    private String email;
    @Basic(optional = false)
    @Column(name = "admission_date")
    private Date admissionDate;
    @Basic(optional = false)
    @Column(name = "area")
    private String area;
    @Basic(optional = false)
    @Column(name = "status")
    private Boolean status;
    @Basic(optional = false)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "registration_date")
    private Date registrationDate;
}
