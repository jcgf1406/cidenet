package co.com.example.employees.mappers;

import co.com.example.employees.entities.Employee;
import co.com.example.employees.models.EmployeeModel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EmployeeMapper {
    Employee toEmployee(EmployeeModel employeeModel);

    EmployeeModel toEmployeeModel(Employee employee);
}
